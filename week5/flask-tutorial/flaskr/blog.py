from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for,
)
import config
import plotly.graph_objects as go
import requests
bp = Blueprint('blog', __name__)


def get_overview(symbol, api_key):
    url = f'https://www.alphavantage.co/query?function=OVERVIEW&symbol={symbol}&apikey={api_key}'
    response = requests.get(url)
    data = response.json()
    return data

def get_globalquote(symbol, api_key):
    try:
        url = f'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol={symbol}&apikey={api_key}'
        response = requests.get(url)
        data = response.json()
        return data
    except KeyError:
        return 'Invalid stock symbol'
    
def get_recent_news(symbol, api_key):
    url = f'https://www.alphavantage.co/query?function=NEWS_SENTIMENT&tickers={symbol}&limit=10&apikey={api_key}&sort=LATEST'
    response = requests.get(url)
    data = response.json()
    return data

def get_timeseries(symbol, api_key):
    url = f'https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol={symbol}&apikey={api_key}'
    response = requests.get(url)
    data = response.json()
    return data


@bp.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        symbol = request.form['stock_symbol']
        over_view = get_overview(symbol, config.API_KEY)
        globoal_quote = get_globalquote(symbol,config.API_KEY)
        news = get_recent_news(symbol, config.API_KEY)
        price_data = get_timeseries(symbol, config.API_KEY)
        dates = list(price_data['Time Series (Daily)'].keys())[:365]
        prices = [float(price_data['Time Series (Daily)'][date]['4. close']) for date in dates]
        fig = go.Figure(data=go.Scatter(x=dates, y=prices))
        fig.update_layout(
            title=f'      {symbol} Stock Price for Past Year(365 days)',
            xaxis_title='Date',
            yaxis_title='Price ($)',
        )

        return render_template('blog/index.html',   symbol = symbol,
                                                    name = over_view['Name'],
                                                    sector =over_view['Sector'],
                                                    industry = over_view['Industry'],
                                                    marketcap = over_view['MarketCapitalization'],
                                                    PE_Ratio = over_view['PERatio'],
                                                    EPS = over_view['EPS'],
                                                    dividend  = over_view['DividendPerShare'],
                                                    dividend_yield = over_view['DividendYield'],
                                                    exchange = over_view['Exchange'],
                                                    weekhigh = over_view['52WeekHigh'],
                                                    weeklow = over_view['52WeekLow'],
                                                    
                                                    price = globoal_quote['Global Quote']['05. price'],
                                                    previous_close = globoal_quote['Global Quote']['08. previous close'],
                                                    open = globoal_quote['Global Quote']['02. open'],
                                                    volume = globoal_quote['Global Quote']['06. volume'],

                                                    news = news['feed'][0:5],
                                                    plot=fig.to_html()
                                                    )
    return render_template('blog/index.html')
