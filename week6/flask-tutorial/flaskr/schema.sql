DROP TABLE IF EXISTS stocks;

CREATE TABLE stocks (
  id INTEGER PRIMARY KEY,
  symbol TEXT NOT NULL,
  date_in DATE NOT NULL,
  tracking_price REAL NOT NULL,
  shares INTEGER NOT NULL,
  current_price REAL,
  percent_change REAL
);
