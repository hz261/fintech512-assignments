from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
import plotly.graph_objs as go
import plotly.utils
from datetime import datetime
from flaskr.db import get_db
import config
import json

bp = Blueprint('blog', __name__)


import requests

# Function to retrieve the current stock price for a given symbol from the Alpha Vantage API
def get_current_price(symbol):
    url = f'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol={symbol}&apikey=<config.API_KEY>'
    response = requests.get(url)
    data = response.json()
    current_price = float(data['Global Quote']['05. price'])
    return current_price

@bp.route('/')
def index():
    db = get_db()
    stocks = db.execute(
        'SELECT id,symbol,date_in,tracking_price,shares'
        ' FROM stocks'
    ).fetchall()
    for stock in stocks:
        symbol = stock['symbol']
        tracking_price = stock['tracking_price']
        shares = stock['shares']

        # Retrieve the current price for the stock
        current_price = get_current_price(symbol)

        # Calculate the percent change
        percent_change = round(((current_price - tracking_price) / tracking_price) * 100,2)

        # Update the table with the new data
        db.execute('UPDATE stocks SET current_price = ?, percent_change = ? WHERE id = ?', (current_price, percent_change, stock['id']))
        db.commit()
        
    stocks = db.execute(
    'SELECT *'
    ' FROM stocks'
    ).fetchall()
    plot_data = [
        go.Pie(
            labels=[s[1] for s in stocks],
            values=[s[5] for s in stocks]
        )
    ]

    # convert plot data to JSON for JavaScript
    plot_data_json = json.dumps(plot_data, cls=plotly.utils.PlotlyJSONEncoder)

    return render_template('blog/index.html', stocks=stocks, plot_data_json=plot_data_json)


@bp.route('/add_stock', methods=('GET', 'POST'))
def add_stock():
    if request.method == 'POST':
        symbol = request.form['symbol']
        tracking_price = request.form['tracking_price']
        shares = request.form['shares']
        error = None
        if not symbol:
            error = 'Symbol is required.'
        elif not tracking_price:
            error = 'Tracking price is required.'
        elif not shares:
            error = 'Shares is required.'
        else:
            # Add the stock to the database
            db = get_db()
            db.execute(
                'INSERT INTO stocks (symbol, date_in, tracking_price, shares) VALUES (?, ?, ?, ?)',
                (symbol, datetime.now().date(), tracking_price, shares)
            )
            db.commit()
            return redirect(url_for('index'))

        flash(error)

    return render_template('blog/add_stock.html')

@bp.route('/<int:id>/remove_stock',methods=('GET', 'POST'))
def remove_stock(id):
    db = get_db()
    db.execute('DELETE FROM stocks WHERE id = ?', (id,))
    db.commit()
    return redirect(url_for('index'))