
var ready = (callback) => {
  if (document.readyState != "loading") callback();
  else document.addEventListener("DOMContentLoaded", callback);
}

ready(() => { 
    /* Do things after DOM has fully loaded */ 
    document.getElementById("submitButton").addEventListener('click',handleClick)
    makeplot("static/stock_data/AAPL.csv")

});

function handleClick(event){
  Swal.fire({
    title: 'Are you sure?',
    text: "You want to submit the page?",
    icon: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Yes',
    cancelButtonText: 'No'
  }).then((result) => {
    if (result.value) {
      Swal.fire(
        'Form Submitted!',
        '',
        'success'
      )
    } else {
      Swal.fire(
        'Action Cancelled',
        '',
        'error'
      )
    }
  })
};

function makeplot(stock_csv) {
  console.log("makeplot: start")
  fetch(stock_csv)
  .then((response) => response.text()) /* asynchronous */
  .catch(error => {
      alert(error)
       })
  .then((text) => {
    console.log("csv: start")
    csv().fromString(text).then(processData)  /* asynchronous */
    console.log("csv: end")
  })
  console.log("makeplot: end")

};

function processData(data) {
    console.log("processData: start")
    let x = [], y = []

    for (let i=0; i<data.length; i++) {
        row = data[i];
        x.push( row['Date'] );
        y.push( row['Close'] );
    } 
    makePlotly( x, y );
    console.log("processData: end")
    }

function makePlotly( x, y ){
    console.log("makePlotly: start")
    var traces = [{
          x: x,
          y: y
      }];
    
    let stock_list = document.getElementById("stock-select");
    let choice = stock_list.options[stock_list.selectedIndex].value;
    if (choice==="AAPL"){
      var layout = { title: "AAPL - Apple Stock Price History"}
    }
    if (choice==="GME"){
      var layout = { title: "GME - GameStop Stock Price History"}
    }
    if (choice==="SPY"){
      var layout = { title: "SPY - S & P 500 Stock Price History"}
    }
    if (choice==="TSLA"){
      var layout = { title: "TSLA - Tesla Stock Price History"}
    }

    myDiv = document.getElementById('myDiv');
    Plotly.react( myDiv, traces, layout );
    console.log("makePlotly: end")
};

function plot_by_choice() {
  let eID = document.getElementById("stock-select");
  let stockVal = eID.options[eID.selectedIndex].value;
  if (stockVal==="AAPL"){
    makeplot("static/stock_data/AAPL.csv")
  }
  else if (stockVal==="GME"){
    makeplot("static/stock_data/GME.csv")
  }
  else if (stockVal==="SPY"){
    makeplot("static/stock_data/SPY.csv")
  }
  else{
    makeplot("static/stock_data/TSLA.csv")
  };
}